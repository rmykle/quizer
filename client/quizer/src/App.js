import React, { Component } from "react";
import Stomp from "stompjs";
import SockJS from "sockjs-client";
import "./scss/App.scss";
import PlayerList from "./components/PlayerList";

class App extends Component {
  componentDidMount() {
    this.socketClient = Stomp.over(new SockJS("http://localhost:8080/ws"));
    this.socketClient.connect(
      {},
      this.onConnect,
      this.onDisconnect
    );
  }

  render() {
    return (
      <div className="app">
        <main>asd</main>
        <PlayerList />
      </div>
    );
  }

  onConnect = () => {
    this.socketClient.subscribe("topic/public", this.onMessageRecieved);
    this.socketClient.send(
      "/quiz/chat.addUser",
      {},
      JSON.stringify({ sender: "bob", type: "JOIN" })
    );
  };
  onDisconnect = () => {};
  onMessageRecieved = () => {
    console.log("chat");
  };
}

export default App;
