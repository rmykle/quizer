export const onConnect = () => {
  this.socketClient.subscribe("topic/public", this.onMessageRecieved);
  this.socketClient.send(
    "/quiz/chat.addUser",
    {},
    JSON.stringify({ sender: "bob", type: "JOIN" })
  );
};
export const onDisconnect = () => {};
export const onMessageRecieved = () => {
  console.log("chat");
};
